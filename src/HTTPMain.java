import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class HTTPMain {

	public static void main(String[] args) throws UnknownHostException, IOException {
		try(Socket s=new Socket("www.horstmann.com", 80);
				InputStream is=s.getInputStream();
				OutputStream os=s.getOutputStream();
				Scanner sc=new Scanner(is);
				PrintWriter pw=new PrintWriter(os, false)){
			    pw.println("GET /index.html HTTP/1.1");
			    pw.println("host: horstmann.com");
			    pw.println("Content-Type: text/html");
			    pw.println("");
			    pw.flush();
			    while(sc.hasNextLine())
			    	System.out.println(sc.nextLine());
		}

	}

}
